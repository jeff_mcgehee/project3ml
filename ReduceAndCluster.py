__author__ = 'Jeff'

import FormatData
import ReduceData
import ClusterData

import numpy as np
import matplotlib.pyplot as plt

from AnalyzeData import ColorList

def reduce_and_cluster(dataset):
    dataset = FormatData.load_heart()
    # dataset = FormatData.loadAd()

    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    for reducer_ind, reducer in enumerate([ReduceData.PCA, ReduceData.ICA,
                                           ReduceData.RCA, ReduceData.LDA]):

        percentages = [.01, .05, .1, .25, .5, .75, 1.]

        nmis = []
        amis = []
        v_measures = []
        for percentage_ind, percentage in enumerate(percentages):

            reduced = reducer(dataset, int(dataset.indim*percentage))

            new_data = dataset.applied_reduction(reduced)

            nmi, ami, v_measure = ClusterData.class_metrics(new_data, ClusterData.Exp_max, show_fig=False)

            nmis.append((np.mean(nmi), np.std(nmi)))
            amis.append((np.mean(ami), np.std(ami)))
            v_measures.append((np.mean(v_measure), np.std(v_measure)))
        nmis = np.asarray(nmis)
        amis = np.asarray(amis)
        v_measures = np.asarray(v_measures)

        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.errorbar(percentages, nmis[:,0], label='NMI', yerr=nmis[:, 1])
        ax.errorbar(percentages, amis[:,0], label='AMI', yerr=amis[:, 1])
        ax.errorbar(percentages, v_measures[:,0], label='V Measure', yerr=v_measures[:, 1])
        ax.legend()


        width = 0.15
        ind = np.arange(len(percentages))

        ax1.bar(ind+0.15*reducer_ind, v_measures[:, 0], width, color=ColorList[reducer_ind], yerr=v_measures[:, 1], label=str(reducer.__name__))
        # ax.bar(ind+width, ad_means, width, color=ColorList[-2], yerr=ad_stds, label="Ad")

        # add some text for labels, title and axes ticks
    ax1.set_ylabel('V Measure')
    ax1.set_xlabel('Percent of Features')
    ax1.grid()
    ax1.set_title(dataset.metadata['name'])
    ax1.set_xticks(ind+0.3)

    tick_labels = ['%d%%'%int(percent*100) for percent in percentages]#['NMI', 'AMI', 'V\nMeasure']
    ax1.set_xticklabels(tick_labels)

    ax1.legend(loc='best')


    plt.show()






if __name__ == "__main__":
    reduce_and_cluster(0)