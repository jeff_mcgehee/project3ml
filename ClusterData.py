__author__ = 'Jeff'

import FormatData

from sklearn.cluster import KMeans

from sklearn.mixture import GMM

from sklearn.metrics import adjusted_mutual_info_score
from sklearn.metrics import normalized_mutual_info_score
from sklearn.metrics import v_measure_score

import time

import matplotlib.pyplot as plt
import numpy as np

from AnalyzeData import ColorList

def K_means(dataset, k_val, n_iter=10):

    outputs = []
    centers = [[] for val in range(k_val)]
    inertias = []

    start = time.time()
    for i in range(n_iter):
        clusterer = KMeans(n_clusters=k_val, n_jobs=4)
        outputs.append(clusterer.fit_predict(dataset.inputs))
        [centers[ind].append(center) for ind, center in enumerate(clusterer.cluster_centers_)]

        inertias.append(clusterer.inertia_)

    print('Time Elapsed:', time.time()-start)


    return inertias, outputs, centers


def Exp_max(dataset, k_val, n_iter=10):

    weights = []
    means = []
    covars = []
    converged = []
    predictions = []
    logprobs = []
    responsibilities = []

    start = time.time()
    for i in range(n_iter):

        clusterer = GMM(k_val)

        clusterer.fit(dataset.inputs)
        weights.append(clusterer.weights_.shape)
        means.append(clusterer.means_.shape)
        covars.append(clusterer.covars_.shape)
        converged.append(clusterer.converged_)
        predictions.append(clusterer.predict(dataset.inputs))

        scoring = clusterer.score_samples(dataset.inputs)

        logprobs.append(scoring[0])
        responsibilities.append(scoring[1])


    print('Time Elapsed:', time.time()-start)

    return converged, predictions, weights, means, covars, logprobs, responsibilities

def responsibilities_plots(dataset, fig_title="Cluster Probability for Various K"):
    k_vals = [dataset.num_classes, 5, 10, 15, 20]
    ind = np.arange(len(k_vals))

    width = 0.5

    means = []
    stds = []

    for k_val in k_vals:
        results = Exp_max(dataset, k_val, n_iter=10)

        means.append(np.mean([np.max(result) for result in results[-1]]))
        stds.append(np.std([np.max(result) for result in results[-1]]))

    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.bar(ind, means, width, color=ColorList[6], yerr=stds)

    # add some text for labels, title and axes ticks
    ax.set_ylabel('Avg. Cluster Probability')
    ax.set_xlabel('K')
    ax.grid()
    ax.set_title(fig_title)
    ax.set_xticks(ind+width/2.)
    tick_labels = [str(val) for val in k_vals]
    tick_labels[0] = tick_labels[0] + '\n(# classes)'
    ax.set_xticklabels(tick_labels)

    plt.show()

def inertia_bars(dataset, fig_title='Inertia for Various K'):

    k_vals = [dataset.num_classes, 5, 10, 15, 20]
    ind = np.arange(len(k_vals))

    width = 0.5

    means = []
    stds = []
    for k_val in k_vals:
        inertia, _, _ = K_means(dataset, k_val, n_iter=10)
        means.append(np.mean(inertia))
        stds.append(np.std(inertia))

    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.bar(ind, means, width, color=ColorList[6], yerr=stds)

    # add some text for labels, title and axes ticks
    ax.set_ylabel('Inertia')
    ax.set_xlabel('K')
    ax.grid()
    ax.set_title(fig_title)
    ax.set_xticks(ind+width/2.)
    tick_labels = [str(val) for val in k_vals]
    tick_labels[0] = tick_labels[0] + '\n(# classes)'
    ax.set_xticklabels(tick_labels)

    plt.show()


#FIXME: pass either K_means or Exp_max
def class_metrics(dataset, cluster_func, show_fig=True):

    results = cluster_func(dataset, k_val=dataset.num_classes, n_iter=10)

    outputs = results[1]

    nmis = []
    amis = []
    v_measures = []
    for output in outputs:

        al_outs, al_targs = align_clusters(output, dataset.targets.astype(int))

        nmis.append(normalized_mutual_info_score(al_targs, al_outs))
        amis.append(adjusted_mutual_info_score(al_targs, al_outs))
        v_measures.append(v_measure_score(al_targs, al_outs))

    if show_fig:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(nmis, color=ColorList[0], label='NMI')
        ax.plot(amis, color=ColorList[1], label="AMI")
        ax.plot(v_measures, color=ColorList[-1], label="V Measure")
        ax.legend(loc="best")
        ax.set_xlabel('Run Number')

        plt.show()

    return nmis, amis, v_measures


def class_metric_analysis(cluster_func, heart_data=None, ad_data=None):

    if not heart_data:
        heart_data = FormatData.load_heart()
    if not ad_data:
        ad_data = FormatData.loadAd()

    heart_out = class_metrics(heart_data, cluster_func, show_fig=False)
    ad_out = class_metrics(ad_data, cluster_func, show_fig=False)

    heart_means = [np.mean(out) for out in heart_out]
    ad_means = [np.mean(out) for out in ad_out]

    heart_stds = [np.std(out) for out in heart_out]
    ad_stds = [np.std(out) for out in heart_out]

    ind = np.arange(len(heart_means))
    width = 0.35

    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.bar(ind, heart_means, width, color=ColorList[6], yerr=heart_stds, label=heart_data.metadata['name'],
           error_kw=dict(ecolor='gray', lw=2, capsize=5, capthick=2))
    ax.bar(ind+width, ad_means, width, color=ColorList[8], yerr=ad_stds, label=ad_data.metadata['name'],
           error_kw=dict(ecolor='gray', lw=2, capsize=5, capthick=2))

    # add some text for labels, title and axes ticks
    ax.set_ylabel('Performance Metric Value')
    ax.set_xlabel('Performance Metric')
    ax.grid()
    ax.set_title(str(cluster_func.__name__)+' Performance')
    ax.set_xticks(ind+width)

    tick_labels = ['NMI', 'AMI', 'V\nMeasure']
    ax.set_xticklabels(tick_labels)

    ax.legend(loc='best')

    plt.show()


def class_hists(dataset, cluster_func):

    results = cluster_func(dataset, k_val=dataset.num_classes, n_iter=1)

    outputs = np.asarray(results[1][0])
    print(outputs.shape)
    print(np.unique(outputs))

    aligned_out, aligned_targs = align_clusters(outputs, dataset.targets)

    ind = np.arange(dataset.num_classes)
    width = 0.35

    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.bar(ind, np.bincount(aligned_out).astype(float)[::-1]/outputs.shape[0], width, color=ColorList[5], label='Clusters')
    ax.bar(ind+width, np.bincount(aligned_targs).astype(float)[::-1]/outputs.shape[0], width, color=ColorList[-2], label='Classes')

    # add some text for labels, title and axes ticks
    ax.set_ylabel('Percent of Members')
    ax.set_xlabel('Class')
    ax.grid()
    ax.set_title('%s Clusters vs. Classes'%dataset.metadata['name'])
    ax.set_xticks(ind+width)

    tick_labels = ind
    ax.set_xticklabels(tick_labels)

    ax.legend()

    plt.show()


def align_clusters(output, targets):

    # classes = np.unique(targets)

    out_bins = np.bincount(output)
    out_args = np.argsort(np.bincount(output))

    targ_bins = np.bincount(targets.astype(int))
    targ_args = np.argsort(np.bincount(targets.astype(int)))

    new_targets = []
    for new_ind, arg_ind in enumerate(targ_args):

        class_vals = [new_ind]*targ_bins[arg_ind]
        [new_targets.append(val) for val in class_vals]

    new_outputs = []
    for new_ind, arg_ind in enumerate(out_args):

        class_vals = [new_ind]*out_bins[arg_ind]
        [new_outputs.append(val) for val in class_vals]

    assert len(new_outputs) == len(new_targets)


    return np.asarray(new_outputs), np.asarray(new_targets)

if __name__ == "__main__":

    dataset = FormatData.loadAd()
    # dataset = FormatData.load_heart()


    # K_means(dataset, k_val=2)
    # Exp_max(dataset)

    # responsibilities_plots(dataset, 'Heart Arrhythmia')
    # inertia_bars(dataset, 'Heart Arrhythmia')

    # class_metrics(dataset)

    class_metric_analysis(Exp_max)

    # class_hists(dataset, Exp_max)