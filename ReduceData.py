__author__ = 'Jeff'

import FormatData

# from sklearn.decomposition import PCA
# from sklearn.decomposition import FastICA

import sklearn.decomposition
import sklearn.lda

# from sklearn.lda import LDA
from sklearn.random_projection import GaussianRandomProjection
from sklearn.decomposition import TruncatedSVD


# Latent Semantic Indexing == scikit truncatedSVD

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy.stats import kurtosis

import time

def reduce_2d(data, reducer, show_fig=True):

    output = reducer(data, 2)
    print(output.shape)

    if show_fig:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        for classval, name in zip(np.unique(data.targets), data.metadata['target_names']):
            ax.scatter(output[data.targets==classval,0],
                       output[data.targets==classval,1], c=np.random.rand(3,), s=60, alpha=0.8, label=name)

        ax.legend(loc='best')#'center left', bbox_to_anchor=(1.0, 0.5))
        ax.set_xlabel('Axis 1')
        ax.set_ylabel('Axis 2')
        ax.set_title(str(reducer.__name__) + ' 2D Reduction on ' + data.metadata['name'])
        plt.show()

    return output


def reduce_3d(data, reducer, show_fig=True):

    output = reducer(data, 3)

    if show_fig:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        for classval, name in zip(np.unique(data.targets), data.metadata['target_names']):
            ax.scatter(output[data.targets==classval,0],
                       output[data.targets==classval,1],
                       output[data.targets==classval,2], c=np.random.rand(3,), alpha=0.8, label=name)

        ax.legend(loc='lower right')#'center left', bbox_to_anchor=(1.0, 0.5))
        plt.show()

    return output
    pass

def PCA(data, n_components, show_fig=False):

    reducer = sklearn.decomposition.PCA(n_components=n_components)
    start = time.time()
    output = reducer.fit_transform(data.inputs)#, data.targets)
    print("Elapsed Time:", time.time()-start)

    eigenvalues = reducer.explained_variance_

    print(eigenvalues.shape)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.hist(eigenvalues)

    if show_fig:
        plt.show()

    return output


def ICA(data, n_components):

    reducer = sklearn.decomposition.FastICA(n_components=n_components, whiten=True)
    start = time.time()
    output = reducer.fit_transform(data.inputs)#, data.targets)

    kurt = kurtosis(output)

    # print(kurt)
    # print(np.mean(kurt))
    print("Elapsed Time:", time.time()-start)

    return output

def RCA(data, n_components):

    reducer = GaussianRandomProjection(n_components=n_components)
    start = time.time()
    reducer.fit(data.inputs)#, data.targets)
    output = reducer.transform(data.inputs)
    print("Elapsed Time:", time.time()-start)

    return output


def LDA(data, n_components):

    reducer = sklearn.lda.LDA(n_components=n_components)
    start = time.time()
    reducer.fit(data.inputs, data.targets)
    output = reducer.transform(data.inputs)
    print("Elapsed Time:", time.time()-start)

    print(output.shape)

    return output

def lsi_analysis(data, n_components):

    reducer = TruncatedSVD(n_components=n_components)
    start = time.time()
    output = reducer.fit_transform(data.inputs)#, data.targets)
    print("Elapsed Time:", time.time()-start)

    return output



if __name__ == "__main__":
    # data = FormatData.load_heart()
    data = FormatData.loadAd()
    # print(np.mean(kurtosis(data.inputs)))
    # PCA(data, data.indim*.04)
    # ICA(data, data.indim*.1)
    # RCA(data)
    # LDA(data)

    reduce_2d(data, LDA)