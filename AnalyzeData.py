__author__ = 'Jeff'

import FormatData


import matplotlib.pyplot as plt
import numpy as np


ColorList = [
            [255./255., 51./255., 51./255.],
            [255./255., 153./255., 51./255.],
            [255./255., 255./255, 51./255],
            [153./255., 255./255, 51./255],
            [51./255., 255./255, 51./255],
            [51./255., 255./255, 153./255],
            [51./255., 255./255, 255./255],
            [51./255., 153./255, 255./255],
            [51./255., 51./255, 255./255],
            [153./255., 51./255, 255./255],
            [255./255., 51./255, 255./255],
            [255./255., 51./255, 153./255],
            [160./255., 160./255, 160./255]
            ]

def load_data():

    ad_data = FormatData.loadAd(noNAN=False)
    ad_data_cross = ad_data.kfold(2)

    religion_data = FormatData.load_religion()
    religion_data_cross = religion_data.kfold(2)

    parkinsons_data = FormatData.load_parkinsons()
    parkinsons_data_cross = parkinsons_data.kfold(2)

    bc_data = FormatData.load_bc()
    bc_data_cross = bc_data.kfold(2)

    heart_data = FormatData.load_heart()
    heart_data_cross = heart_data.kfold(2)

    return {'ads':(ad_data, ad_data_cross), 'bc':(bc_data, bc_data_cross),
            'religion':(religion_data, religion_data_cross), 'parkinsons':(parkinsons_data, parkinsons_data_cross),
            'heart':(heart_data, heart_data_cross)}


def dataset_analysis(dataset, colorList=None):

    reordered_inds = np.argsort(dataset.targets)

    ordered_targets = dataset.targets[reordered_inds]

    print('Target Info:')
    bincounts = np.bincount(dataset.targets.astype(int))
    for ind, count in enumerate(bincounts):
        print('Feature %d: %0.2f'%(ind, 100.*count/dataset.targets.shape[0]))

    targ_fig = plt.figure()
    targ_ax = targ_fig.add_subplot(111)

    targ_ax.grid()

    targ_ax.hist(dataset.targets.astype(int), color=colorList[7], normed=True)
    targ_ax.set_xlabel('Class')
    targ_ax.set_ylabel('Percent Occurrence')
    targ_ax.set_title(dataset.metadata['name']+' Class Distribution')


    ordered_inputs = dataset.inputs[reordered_inds]

    ordered_targets = np.asarray([ordered_targets]).T
    ordered_data = np.append(ordered_inputs, ordered_targets, axis=1)



    class_features = []
    for answer in np.unique(ordered_targets):
        class_features.append(ordered_data[ordered_data[:, -1].astype(int) == answer, :])


    avgs_fig = plt.figure()
    avgs_ax = avgs_fig.add_subplot(111)



    for ind in range(ordered_inputs.shape[1]):
        fig = plt.figure()

        fig.suptitle((dataset.metadata['input_names'][ind] +': ' + 'Importance-%0.2f')%dataset.importances[ind])
        ax = fig.add_subplot(111)


        avgs = []
        hist_data = []
        hist_labels = []
        colors = []
        for class_ind, class_set in enumerate(class_features):

            # ax = fig.add_subplot(1, len(class_features), class_ind)
            # ax.set_title(dataset.metadata['target_names'][class_ind])

            reformed_features = class_set[:,:].T


            # print(reformed_features[-1,:])

            hist_data.append(reformed_features[ind, :])
            hist_labels.append(dataset.metadata['target_names'][class_ind])

            # ax.hist(reformed_features[ind, :], alpha=0.4,stacked=True, normed=True, label=str(class_ind))

            avgs.append(np.mean(reformed_features[ind, :]))

            if colorList:
                colors.append(colorList[class_ind])


        if colorList:
            ax.hist(hist_data, alpha=0.7,stacked=True, normed=False, color=colors, label=hist_labels)

            # avgs_ax.plot(range(len(avgs)),avgs, c = colors[ind],
            #                 label=(dataset.metadata['input_names'][ind]
            #                           +': ' + 'Importance-%0.2f')%dataset.importances[ind])

        else:
            ax.hist(hist_data, alpha=0.7,stacked=False, normed=False, label=hist_labels, tight=True)

        avgs_ax.plot(range(len(avgs)),avgs,
                        label=(dataset.metadata['input_names'][ind]
                                  +': ' + 'Importance-%0.2f')%dataset.importances[ind])

        avgs_ax.legend(loc='best')

        ax.set_xlabel('Scaled Value')
        ax.set_ylabel('Occurrences')
        ax.grid()
        ax.legend(loc='best')


    avgs_ax.set_xlabel('Classes')
    avgs_ax.set_ylabel('Avg. Value')
    avgs_ax.set_title('Averages of Features for Each Class')


    plt.show()


if __name__ == '__main__':

    datasets = load_data()


    # data = datasets['ads']
    data = datasets['heart']

    importance_thresh = 0.04
    data = data[0].reduced_inputs(importance_thresh)

    print('Number of Features with Importance > %0.1f: %d'%(importance_thresh*100., data.indim ))

    dataset_analysis(data, colorList)